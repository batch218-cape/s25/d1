// console.log("Hello World");

// [SECTION]  JSON Objects
/*
    - JSON stands for JavaScript Object Notation
    - A common use of JSON is to read data from a web server, and display the data in a web page
    - Features of JSON:
        - lightweight data-interchange format
        - easy to read and write
        - easy for machine to parse and generate
*/

//JSON Objects
// - JSON also use the "key/value pairs" just like the object properties in Javascript
// - "key/ property" names requires to be enclosed with double quotes

/*
	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "value"
	}
*/

// Example of JSON object
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines"
// }

//Arrays of JSON Object

// "cities" : [
//     {
//         "city": "Quezon City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     }
//     {
//         "city": "Manila City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     }
//     {
//         "city": "Makati City",
//         "province": "Metro Manila",
//         "country": "Philippines"
//     }
// ]

console.log("--------")
// JavaScript Array of Objects
let batceshArr = [
    {
        batchName: "Batch 218",
        schedule: "Part Time"
    },
    {
        batchName: "Batch 21",
        schedule: "Full Time"
    }
]
console.log(batceshArr);

// The "stringify" method is used to convert JavaScript Objects into a string
console.log("Result of stringify method: ");
console.log(JSON.stringify(batceshArr));

let data = JSON.stringify({
    name: "John",
    age: 31,
    address: {
        city: "Manila",
        country: "Philippines"
    }
})
console.log(data);

// global variables
// let firstname = prompt("Enter your first name: ");
// let lastname = prompt("Enter your last name: ");
// let email = prompt("Enter your email: ");
// let password = prompt("Enter your password: ");

let firstname = "Avery";
let lastname = "Arreola";
let email = "averyarreola@gmail.com";
let password = "brotherisamazing123";

let otherData = JSON.stringify({
    firstname: firstname,
    lastname: lastname,
    email: email,
    password, password
})
console.log(otherData);

// [SECTION] Converting stringified JSON into Javascript Objects

let batchesJSON = `[
    {
        "batchName": "Batch 218",
        "schedule": "Part time"
    },
    {
        "batchName": "Batch 219",
        "schedule": "Full time"
    }
]`
console.log("batchesJSON content: ")
console.log(batchesJSON);

// JSON.parse method converts JSON Object into Javascript Object
console.log("Result from parse method: ")
// console.log(JSON.parse(batchesJSON));

/*
    let parseBatches = [
        {
            "batchName": "Batch 218",
            "schedule": "Part time"
        },
        {
            "batchName": "Batch 219",
            "schedule": "Full time"
        }
    ]
*/
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
    "name": "John",
    "age": 31,
    "address": {
        "city": "Manila",
        "country": "Philippines"
    }
}`
console.log(stringifiedObject);
// Mini-Activity
// Convert stringifiedObject to a Javascript Object
console.log(JSON.parse(stringifiedObject));


